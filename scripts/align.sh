for i in output/trim/*.fastq.gz
do
  bn=`basename $i .gz`
  libname=`basename $i .fastq.gz`
  gunzip $i
  bowtie --best --strata -k 1 -m 1 -S output/bowtie-index/mirna output/trim/$bn > output/alignment/$libname.sam
  samtools view -bS output/alignment/$libname.sam | samtools sort - output/alignment/$libname
  gzip output/trim/$bn
done
