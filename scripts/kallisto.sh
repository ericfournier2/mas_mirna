for i in output/trim/*.fastq.gz
do
  libname=`basename $i .fastq.gz`
  mkdir -p output/kallisto/$libname
  kallisto quant -i output/bta_ensembl.idx -o output/kallisto/$libname -b 100 --single -l 180 -s 20 $i
done
