cat > output/trim/adapters.fa << END
>Single
TGGAATTCTCGGGTGCCAAGGAACTCCAGTCACTGA
END

mkdir -p output/trim
for lib in raw/*.fastq.gz
do
  bn=`basename $lib _R1.fastq.gz`
  sn=`echo $bn | sed -e 's/.*Index_.*\.//'`
  java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
    -threads 6 \
    -phred33 \
    $lib \
    output/trim/$sn.fastq.gz \
    ILLUMINACLIP:output/trim/adapters.fa:2:30:15 \
    MINLEN:10 \
    2> output/trim/$sn.fastq.gz.log
done
